package edu.usyd.au.fitness.domain;

import junit.framework.TestCase;


import edu.usyd.au.fitness.domain.Award;

public class AwardTest extends TestCase {

    private Award award;

    protected void setUp() throws Exception {
    	award = new Award();
    }

    public void testSetAndGetUserName() {
        String testUserName = "testUser";
        assertNull(award.getUserName());
        award.setUserName(testUserName);
        assertEquals(testUserName, award.getUserName());
    }

    public void testSetAndGetGold() {
        int testGold = 10;
        assertEquals(0, 0, 0);    
        award.setGold(testGold);
        assertEquals(testGold,award.getGold() , 0);
    }
  
    
    public void testSetAndGetSilver() {
        int testSilver = 10;
        assertEquals(0, 0, 0);    
        award.setSilver(testSilver);
        assertEquals(testSilver,award.getSilver() , 0);
    }
    
    public void testSetAndGetBronze() {
        int testBronze = 10;
        assertEquals(0, 0, 0);    
        award.setBronze(testBronze);
        assertEquals(testBronze,award.getBronze() , 0);
    }
    
    public void testSetAndGetCredit() {
        int testCredit = 10;
        assertEquals(0, 0, 0);    
        award.setCredit(testCredit);
        assertEquals(testCredit,award.getCredit() , 0);
    }
   
}
