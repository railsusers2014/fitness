package edu.usyd.au.fitness.domain;

import java.util.Date;

import junit.framework.TestCase;
import edu.usyd.au.fitness.domain.Result;

public class ResultTest extends TestCase {

    private Result result;
    
    protected void setUp() throws Exception {
    	result = new Result();
    }

    public void testSetAndGetUserName() {
        String testUserName = "testUser";
        assertNull(result.getUserName());
        result.setUserName(testUserName);
        assertEquals(testUserName, result.getUserName());
    }
    
    public void testSetAndGetDate() {
        Date testDate = new Date();
        assertNull(result.getDate());
        result.setDate(testDate);
        assertEquals(testDate, result.getDate());
    }
  
    public void testSetAndGetScore() {
        int testScore = 10;
        assertEquals(0, 0, 0);    
        result.setScore(testScore);
        assertEquals(testScore,result.getScore() , 0);
    }
    
    public void testSetAndGetLevel() {
        String testLevel= "green";
        assertNull(result.getLevel());
        result.setLevel(testLevel);
        assertEquals(testLevel, result.getLevel());
    }
}
