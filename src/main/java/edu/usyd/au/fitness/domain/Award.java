package edu.usyd.au.fitness.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="AWARD")
@SequenceGenerator(name="seq_user", initialValue=100001, allocationSize=100,sequenceName="seq_user")
public class Award implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column(name="USER_NAME", length = 20)
	private String userName;
	@Column(name="Gold", length = 20)
	private int gold;
	@Column(name="Silver", length = 20)
	private int silver;
	@Column(name="Bronze", length = 20)
	private int bronze;
	@Column(name="Credit", length = 20)
	private int credit;
	
	@Column(name="Rank", length = 20)
	private int rank;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public int getGold() {
		return gold;
	}
	public void setGold(int gold) {
		this.gold = gold;
	}
	
	public int getSilver() {
		return silver;
	}
	public void setSilver(int silver) {
		this.silver = silver;
	}
	
	public int getBronze() {
		return bronze;
	}
	public void setBronze(int bronze) {
		this.bronze = bronze;
	}
	
	public int getCredit() {
		return credit;
	}
	public void setCredit(int credit) {
		this.credit = credit;
	}

	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}

	
}
