package edu.usyd.au.fitness.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.usyd.au.fitness.domain.User;

@Service(value="LoginInput")
@Transactional
public class LoginInput {
	
	private SessionFactory sessionFactory;
	
	protected final Log logger =LogFactory.getLog(getClass());
	
	private String LoginId;
	private String password;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public void loadInput(User user)
	{
		LoginId = user.getEmail();
		password = user.getPassword();
	}
	
	
	
	
//	public void setLoginId(String id)
//	{
//		LoginId = id;
//		logger.info("LoginId set to " + id);
//	}
//	
//	public void setPassword(String pwd)
//	{
//		password = pwd;
//		logger.info("Password set to " + pwd);
//	}
	
	public String getLoginId()
	{
		return LoginId;
	}
	
	public String getPassword()
	{
		return password;
	}
	
//	public List<User> getUserByInput(User input)
//	{
//		Session currentSession = this.sessionFactory.getCurrentSession();
////		User user = (User) currentSession.get(User.class, input.getEmail());
//		
//		List<User> users = currentSession.createQuery("FROM User").list();
//		
//		return users;
//	}



		

}	
	
	

