<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validator.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/enquire.js'/>"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>
    <div class="content">
      <div class="content_bg">
        <div class="mainbar">
          <div class="article">
            <div class="clr"></div>
          </div>
          <div class="article">
          <table id="mytable" cellspacing="0">
            <form id="saveReplied" action="/enquirySystem/admin/saveReplied" method="post">
            	<tr>
            		<th>ID</th>
            		<th>ACTIVITYNAME</th>
            		<th>DETAILS</th>
            		<th>UNIT</th>
            		<th>CALS</th>
            		<th>CREATEDDATE</th>
            	</tr>
            	<c:forEach items="${model.activities}" var="activity">
				<tr>
            		<th class="specalt" abbr="Model" scope="row">${activity.id}</th>
            		<td class="">${activity.name}</td>
            		<td class="">${activity.details}</td>
            		<td class="">${activity.unit}</td>
            		<td class="">${activity.cals}</td>
            		<td class=""><fmt:formatDate type="date" pattern="dd/MM/yyyy" value="${activity.createdDate}" /></td>
            	</tr>
				</c:forEach>
				<c:if test="${model.activities.size() eq 0}">
					<tr><td colspan="6" align="center">No Activities results for the Filer</td></tr>
				</c:if>
				<c:if test="${model.activities.size() > 0}">
					<tr><td colspan="6" align="left"> Totel Records: ${model.activities.size()}</td></tr>
				</c:if>
            </table>
          </div>
          <div class="pagenavi"><span class="pages"> </span></div>
        </div>
        <%@ include file="/WEB-INF/views/menu.jsp"%>
        <script type="text/javascript">$("#ACTIVITYRECORDS").addClass("active")</script>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
