<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validator.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/enquire.js'/>"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>
    <div class="content">
      <div class="content_bg">
        <div class="mainbar">
          <div class="article">
            <div class="clr"></div>
            <div class="clr"></div>
          </div>
          <div class="article">
          	<form  action="evaluation/result"  method="post" 
          	onsubmit="return confirm('Please check the data again.The evaluation can not be changed!!!\nAre you sure to submit it?');">
          <div>
         <input type="date" name="wdate" id="time1" onclick="MyCalendar.SetDate(this)" value="29/10/2013" />
      	  </div>
  		
          		<fieldset>
			<legend>Calories</legend> 
            	 <label for="planRate">Did you follow your plan today?</label><br>
                 <select name="planRate">
                    <option value="">Select</option>
	                <option value="90%-100%">90%-100%</option>
                    <option value="80%-89%">80%-89%</option>
                    <option value="70%-79%">70%-79%</option>
                    <option value="60%-69%">60%-69%</option>
                    <option value="below 60%">below 60%</option>
	              </select><br>
                  <label for="exercise">Did you take exercise today?</label><br>
                 <select name="exerciseType">
                    <option value="">Select</option>
	                <option value="None">None</option>
                    <option value="Light">Light</option>
                    <option value="Normal">Normal</option>
                    <option value="Vigorous">Vigorous</option>
	              </select>
	               <label for="fortime"> for </label>
	               <input id="hours" name="exerciseHour" type="number" class="text"  size="7" min="0" step="1" pattern="\d+"/>
	               <label for="hours"> Hours </label><br>
                   <label for="cal">Please fill your calorie intake today</label><br/>
                   <input id="cal" name="caloriesIntake" type="number" class="text"  size="7" min="0" step="1" pattern="\d+"/>
                   <label for="c">CALs</label><br>
                </fieldset>sss
                
                 <fieldset>
			<legend>Health</legend> 
            	  <label for="goal">Did you reach the goal today?</label><br/>
            	  <label for="yes"> <input type="radio" checked="checked" name="goalReach" value="yes" id="yes1" /> Yes</label>
            	  <label for="no"> <input type="radio" name="goalReach" value="no" id="no0" /> No</label><br/>
                  <label for="sitting">How long did you sit today?</label><br>
	              <input id="st" name="sittingTime" type="number" class="text"  size="7"/>
	              <label for="hours"> Hours </label><br>
                  <label for="sleep">How long did you sleep today?</label><br>
	              <input id="sp" name="sleepingTime" type="number" class="text"  size="7"/>
	              <label for="hours"> Hours </label><br>
                </fieldset><br/>
			<input type="submit" value="Calculate"/>
			</form>
          </div>
          <div class="pagenavi"><span class="pages"> </span></div>
        </div>
        <%@ include file="/WEB-INF/views/menu.jsp"%>
        <script type="text/javascript">$("#EVALUATION").addClass("active");</script>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
