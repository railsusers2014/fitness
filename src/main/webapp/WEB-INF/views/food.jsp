<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validator.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/enquire.js'/>"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>
    <div class="content">
      <div class="content_bg">
        <div class="mainbar">
          <div class="article">
            <div class="clr"></div>
            <c:if test="${isAddedSuc}">
          		<class="post-data">A record had been added successfully.</p>
            </c:if>
            <div class="clr"></div>
          </div>
          <div class="article">
            <!--<form:form modelAttribute="food" method="POST" action="saveFood" id="foodForm">
              <ol>
              	<li>
                  <label for="foodName">Food Name<font class="red">*</font></label>
                  <input id="foodName" name="foodName" type="text" class="text" />
                </li>
                <li>
                  <label for="amount">AMOUNT <font class="red">*</font></label>
                  <input id="amount" name="amount" type="text" class="text" />
                </li>
                <li>
                  <label for="unit">UNIT <font class="red">*</font></label>
                  <input id="unit" name="unit" type="text" class="text" />
                </li>
                <li>
                  <label for="cals">CALS <font class="red">*</font></label>
                  <input id="cals" name="cals" type="text" class="text" />
                </li>
                 <li>
                  <label for="fat">FAT <font class="red">*</font></label>
                  <input id="fat" name="fat" type="text" class="text" />
                </li>
                <li>
                  <label for="carbs">CARBS <font class="red">*</font></label>
                  <input id="carbs" name="carbs" type="text" class="text" />
                </li>
                <li>
                  <label for="prot">PROT <font class="red">*</font></label>
                  <input id="prot" name="prot" type="text" class="text" />
                </li>
                <li>
                  <input value="Submmit" type="submit" name="imageField" id="imageField" src="${pageContext.request.contextPath}/resources/images/submit.gif" class="send" />
                  <div class="clr"></div>
                </li>
              </ol>
                </form:form>-->
          
          <form:form method="POST" action="foodSearch" id="foodForm">
          
          <input type="text" name="foodName"/>
          <input type="submit" value="Search"/><br>
          <c:out value="${model.nofind}" />                 
          </form:form>
          
     <c:if test="${model.isSelect}">
     	<div>
     	<table>
     	<tr>    	<td>
		  <form:form modelAttribute="food" method="POST" action="saveFood" id="foodForm">		  
		  <label>Food name:</label>
     	  <input id="foodName" name="foodName" type="text" value="${model.foodinf.food_name}" readonly="readonly"/>
     	  </tr>    	</td>
     	  <tr>    	<td>
     	  <label>Serving(g):</label>
     	  <input id="amount" name="amount" type="text" value="${model.foodinf.number_of_units}" />
     	  </tr>    	</td>
     	  <label>Unit:</label>
     	  <input id="unit" name="unit" type="hidden" value="${model.foodinf.metric_serving_unit}" /> 
     	  <tr>    	<td>	  
     	  <label>Calories:</label>
     	  <input id="cals" name="cals" type="text" value="${model.foodinf.calories}" readonly="readonly"/>
     	  </tr>    	</td>
     	  <tr>    	<td>
     	  <label>Fat:</label>
     	  <input id="fat" name="fat" type="text" value="${model.foodinf.fat}" readonly="readonly"/>
     	  </tr>    	</td>
<!--       	  <label>Carbohydrate:</label> -->
     	  <input id="carbs" name="carbs" type="hidden" value="${model.foodinf.carbohydrate}" />
<!--      	  <label>Protein:</label> -->
     	  <input id="prot" name="prot" type="hidden" value="${model.foodinf.protein}" />
<!--      	  <label>Cholesterol:</label> -->
     	  <input id="chol" name="chol" type="hidden" value="${model.foodinf.cholesterol}" />
<!--      	  <label>Sodium:</label> -->
     	  <input id="sodium" name="sodium" type="hidden" value="${model.foodinf.sodium}" />
<!--      	  <label>Fiber:</label> -->
     	  <input id="fiber" name="fiber" type="hidden" value="${model.foodinf.fiber}" />
<!--      	  <label>Sugar:</label> -->
     	  <input id="sugar" name="sugar" type="hidden" value="${model.foodinf.sugar}" />
<!--      	  <label>Calcium:</label> -->
     	  <input id="calcium" name="calcium" type="hidden" value="${model.foodinf.calcium}" />
<!--      	  <label>Iron:</label> -->
     	  <input id="iron" name="iron" type="hidden" value="${model.foodinf.iron}" />
     	  <tr>    	<td>
     	  <input type="submit" value="Add"/> 
     	  </tr>    	</td>
     	  </form:form>
     	  </table>
         </div>
       </c:if>
       <div>
          <table id="mytable" cellspacing="0">
          <label>Your Food Log:</label>
		  <tr>
            <th>FOODNAME</th>
            <th>Serving(s)</th>
            <th>Calories</th>
          </tr>
          <c:set value="0" var="sum" />
          <c:forEach items="${model.userfoodInfo}" var="foods" varStatus="status">
				<tr>
            		<td class="">${foods.food.foodName}</td>
            		<td class="">${foods.amount}</td>
            		<td class="">${foods.amount*foods.food.cals}</td>     
					<c:set value="${sum + foods.amount*foods.food.cals}" var="sum" />                		
            	</tr>
          </c:forEach>
          <tr>
          <td></td>
          <td></td>
          <td>Total: ${sum} </td>
          </tr>
          </table>
       </div>
      </div>
          <div class="pagenavi"><span class="pages"> </span></div>
        </div>
        <%@ include file="/WEB-INF/views/menu.jsp"%>
        <script type="text/javascript">$("#FOOD").addClass("active")</script>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
