<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validator.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/enquire.js'/>"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>
    <div class="content">
      <div class="content_bg">
        <div class="mainbar">
          <div class="article">
            <div class="clr"></div>
          </div>
          <div class="article">
          <table id="mytable" cellspacing="0">
            <form id="saveReplied" action="/enquirySystem/admin/saveReplied" method="post">
            	<tr>
            		<th>ID</th>
            		<th>FOODNAME</th>
            		<th>AMOUNT</th>
            		<th>UNIT</th>
            		<th>CALS</th>
            		<th>FAT</th>
            		<th>CARBS</th>
            		<th>PROT</th>
            		<th>CREATEDDATE</th>
            	</tr>
            	<c:forEach items="${model.foods}" var="food">
				<tr>
            		<th class="specalt" abbr="Model" scope="row">${food.id}</th>
            		<td class="">${food.foodName}</td>
            		<td class="">${food.amount}</td>
            		<td class="">${food.unit}</td>
            		<td class="">${food.cals}</td>
            		<td class="">${food.fat}</td>
            		<td class="">${food.carbs}</td>
            		<td class="">${food.prot}</td>
            		<td class=""><fmt:formatDate type="date" pattern="dd/MM/yyyy" value="${food.createdDate}" /></td>
            	</tr>
            	<c:if test="${model.foods.size() eq 0}">
					<tr><td colspan="9" align="center">No Activities results for the Filer</td></tr>
				</c:if>
				<c:if test="${model.foods.size() > 0}">
					<tr><td colspan="9" align="left"> Totel Records: ${model.foods.size()}</td></tr>
				</c:if>
				</c:forEach>
            </table>
          </div>
          <div class="pagenavi"><span class="pages"> </span></div>
        </div>
        <%@ include file="/WEB-INF/views/menu.jsp"%>
        <script type="text/javascript">$("#FOODRECORDS").addClass("active")</script>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
